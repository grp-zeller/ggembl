#!/usr/bin/Rscript

#' EMBL palette
#'
#' EMBL colour palette
#' @export
embl_color_pal <- function(palette = "EMBL 8",
                           type = c("regular", "sequential",
                                    "diverging"),
                           direction = 1) {
  type <- match.arg(type)
  palettes <- ggembl::embl.palette.data[[type]]
  if (!palette %in% names(palettes)) {
    stop("`palette` must be one of ", paste(names(palettes), collapse = ", "),
         ".")
  }
  value <- palettes[[palette]][["value"]]
  max_n <- length(value)
  f <- function(n) {
    if (n > max_n){
      warning("This pallete can handle a maximum of ", max_n, ' value. ',
              'You have supplied ', n, '.')
    } else if (n < 0){
      stop("`n` must be a non-negative integer")
    }
    value <- value[seq_len(n)]
    if (direction < 0) {
      value <- rev(value)
    }
    value
  }
  attr(f, "max_n") <- length(value)
  f
}

#' @export
scale_colour_embl <- function(palette = "EMBL 8",
                                 type = "regular",
                                 direction = 1,
                                 ...) {
  discrete_scale("colour", "embl",
                 embl_color_pal(palette, type, direction), ...)
}

#' @export
scale_fill_embl <- function(palette = "EMBL 8",
                               type = "regular",
                               direction = 1,
                               ...) {
  discrete_scale("fill", "embl",
                 embl_color_pal(palette, type, direction), ...)
}

#' @export
scale_color_embl <- scale_colour_embl

#' @export
embl_gradient_pal <- function(palette = "Green",
                              type = "sequential") {
  type <- match.arg(type, c("sequential", "diverging"))
  pals <- ggembl::embl.palette.data[[type]]
  if (!palette %in% names(pals)) {
    stop("`palette` must be one of ", paste(names(pals), collapse = ", "),
         ".")
  }
  pal <- pals[[palette]]
  scales::gradient_n_pal(colours = pal[["value"]])
}

#' @export
embl_seq_gradient_pal <- function(palette = "Green", ...) {
  embl_gradient_pal(palette = palette, type = "sequential", ...)
}

#' @export
embl_div_gradient_pal <- function(palette = "Red-Blue",  ...) {
  embl_gradient_pal(palette = palette, type = "diverging", ...)
}

#' @export
scale_colour_gradient_embl <- function(palette = "Green",
                                       type='sequential',
                                       ...,
                                       na.value = "grey50",
                                       guide = "colourbar") {
  if (type=='sequential'){
    continuous_scale("colour", "embl",
                     embl_seq_gradient_pal(palette),
                     na.value = na.value,
                     guide = guide,
                     ...)
  } else if (type=='diverging'){
    continuous_scale("colour", "embl",
                     embl_div_gradient_pal(palette),
                     na.value = na.value,
                     guide = guide,
                     ...)
  } else {
    stop("unknown palette type")
  }
}

#' @export
scale_fill_gradient_embl <- function(palette = "Green",
                                     ...,
                                     na.value = "grey50",
                                     guide = "colourbar") {
  continuous_scale("fill", "embl",
                   embl_seq_gradient_pal(palette),
                   na.value = na.value,
                   guide = guide,
                   ...)
}

#' @export
scale_color_gradient_embl <- scale_colour_gradient_embl

#' @export
scale_color_continuous_embl <- scale_colour_gradient_embl

#' @export
scale_fill_continuous_embl <- scale_fill_gradient_embl

#' @export
scale_colour_gradient2_embl <- function(palette = "Red-Blue",
                                           ...,
                                           na.value = "grey50",
                                           guide = "colourbar") {
  continuous_scale("colour", "embl",
                   embl_div_gradient_pal(palette),
                   na.value = na.value,
                   guide = guide,
                   ...)
}

#' @export
scale_fill_gradient2_embl <- function(palette = "Red-Blue",
                                         ...,
                                         na.value = "grey50",
                                         guide = "colourbar") {
  continuous_scale("fill", "embl",
                   embl_div_gradient_pal(palette),
                   na.value = na.value,
                   guide = guide,
                   ...)
}

#' @export
scale_color_gradient2_embl <- scale_colour_gradient2_embl
