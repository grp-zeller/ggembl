#!/usr/bin/Rscript

#' Basic EMBL theme
#'
#' Basic theme for EMBL style figures
#'
#' @export
theme_embl <- function(font.size = 12, line.size=0.45, panel.grid='none') {
  if (!panel.grid %in% c('none', 'major', 'full', 'major_x', 'major_y')){
    warning("Unrecognized panel.grid info... Reverting to 'none'")
    panel.grid <- 'none'
  }
  thm <- theme_bw() +
    theme(line = element_line(colour = "black",
                              lineend = "square",
                              linetype = "solid",
                              size=line.size),
          rect = element_rect(fill = NA,
                              colour = "black",
                              linetype = "solid",
                              size=line.size),
          text = element_text(colour = "black",
                              face = "plain",
                              size = font.size,
                              vjust = 0.5,
                              hjust = 0.5,
                              lineheight = 1),
          plot.background = element_blank(),
          panel.background = element_blank(),
          panel.border = element_rect(fill=NA, colour='black', size=line.size),
          strip.background = element_blank(),
          legend.key = element_blank(),
          title = element_text(size = rel(1)),
          plot.title = element_text(size = rel(1.2), face = "bold"),
          strip.text = element_text(colour='black'),
          axis.ticks.length = unit(0.3, "lines"),
          axis.text = element_text(size=rel(0.8), colour='black'),
          axis.ticks = element_line(colour='black'))
  if (panel.grid=='none'){
    thm <- thm + theme(panel.grid = element_blank())
  } else if (stringr::str_detect(panel.grid,'major')){
    thm <- thm + theme(panel.grid = element_blank())
    if (panel.grid=='major') {
      thm <- thm + theme(panel.grid.major = element_line(colour='lightgrey',
                                                         size=0.6*line.size))
    } else if (panel.grid=='major_x'){
      thm <- thm + theme(panel.grid.major.x = element_line(colour='lightgrey',
                                                           size=0.6*line.size))
    } else if (panel.grid =='major_y'){
      thm <- thm + theme(panel.grid.major.y = element_line(colour='lightgrey',
                                                           size=0.6*line.size))
    }
  } else if (panel.grid=='full'){
    thm <- thm + theme(panel.grid.minor = element_line(colour='lightgrey',
                                                       size=0.3*line.size),
                       panel.grid.major = element_line(colour='lightgrey',
                                                       size=0.6*line.size))
  }
  return(thm)
}

#' @export
theme_publication <- function(panel.grid='none'){
  theme_embl(font.size = 8, line.size=0.45, panel.grid=panel.grid)
}

#' @export
theme_presentation <- function(panel.grid='none'){
  theme_embl(font.size=12, line.size=0.7, panel.grid=panel.grid)
}

#' @export
theme_poster <- function(panel.grid='none'){
  theme_embl(font.size=16, line.size=1, panel.grid=panel.grid)
}
