# ggembl

This package provides EMBL-styled themes for `ggplot` figures. This includes a
theme for publication-ready figure panels and a colour palette with
the official EMBL colours.

## Installation

In order to install the package, either
+ clone the repository and use R to install it
```bash
git clone https://git.embl.de/grp-zeller/ggembl.git
R CMD INSTALL ggembl
```
+ or install it via `devtools`
```R
require(devtools)
devtools::install_git('https://git.embl.de/grp-zeller/ggembl.git',
    credentials = git2r::cred_user_pass("username", getPass::getPass()))
```

## Usage

The package provides two major features:
+ a consistent and clean theme for `ggplot` figures
+ EMBL colour scales

After loading the package, you can simply use one of the themes and the
colour scales in a normal `ggplot` command.

```R
library("tidyverse")
library("ggembl")

iris %>%
    ggplot(aes(x=Sepal.Length, y=Sepal.Width, col=Species)) +
    geom_point() +
    theme_publication() +
    scale_colour_embl()
```
![](./man/figures/iris.png)


### Themes

There are three different themes:
+ `theme_publication`: line width for plot elements is __1pt__ (and scaled down
    for panel grid lines), text size is __8pt__ for axis labels and 6pt for tick
    labels
+ `theme_presentation`: line width for plot elements is __1.5pt__, text size is
    __12pt__ for axis labels and 10pt for tick labels
+ `theme_poster`: line width for plot elements is __2.1pt__, text size is is
    __16pt__ for axis labels and 12pt for tick labels

In the themes, you can specify if you want a panel grid, which can be either
+ `full` (both minor and major panel grids)
+ `major` (only major panel grids)
+ `major_x` and `major_y` (major panel grid in only one dimension)
+ `none` (default, no panel grid is drawn)


### Colours

The package includes several colour palettes:

#### Regular Palettes

+ EMBL 8
![](./man/figures/palette_embl8.png)

+ EMBL 24
![](./man/figures/palette_embl24.png)

#### Divering Palettes

All divergent gradient colour palettes are also available as reverse versions,
e.g. `Blue-Red` and `Red-Blue`

+ Blue-Red
![](./man/figures/palette_Blue-Red.png)

+ Blue-Orange
![](./man/figures/palette_Blue-Orange.png)

+ Green-Orange
![](./man/figures/palette_Green-Orange.png)

+ Grey-Red
![](./man/figures/palette_Grey-Red.png)

+ Purple-Yellow
![](./man/figures/palette_Purple-Yellow.png)

#### Sequential Palettes

+ Green
![](./man/figures/palette_Green.png)

+ Red
![](./man/figures/palette_Red.png)

+ Blue
![](./man/figures/palette_Blue.png)

+ Orange
![](./man/figures/palette_Orange.png)

+ Purple
![](./man/figures/palette_Purple.png)

+ Grey
![](./man/figures/palette_Grey.png)

## Contact

If you have questions, suggestions, or problems please feel free to contact
[me](mailto:jakob.wirbel@embl.de) or open an issue in this repository.
